import errorHandler from "errorhandler";

import app from "./app";

/**
 * Error Handler. Provides full stack - remove for production
 * 错误处理。为生产提供完整的堆栈移除
 */
app.use(errorHandler());

/**
 * Start Express server.
 * 通过 app.ts 文件中得app.set("port", process.env.PORT || 3000); 设置了 port 值
 */
const server = app.listen(app.get("port"), () => {
  console.log(
    "  App is running at http://localhost:%d in %s mode",
    app.get("port"),
    app.get("env")
  );
  console.log("  Press CTRL-C to stop\n");
});

// 通过npm run start 启动
export default server;
